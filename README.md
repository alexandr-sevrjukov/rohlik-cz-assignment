# Rohlik.cz Candidate Assignment

## Introduction
This is a homework assignment for java developer job candidates.

The application is implemented using Spring Boot 2 and uses Apache Maven as a build and dependency
management tool. The build produced an executable `jar` which contains Tomcat server instance

## Building and Running
Pre-requisites:

* maven 3.6.x
* JDK 11

How to build the app:

```
mvn clean package
```

To run the app, navigate to the `target` directory and execute this command:

```
java -jar assignment-1.0.jar
```
The application is now running and listening on port `8080`. In case that `8080` port is already being used by another 
application, you can specify another port using a switch:

```
java -jar assignment-1.0.jar --server.port=8083
```

## Testing
In the `test` directory you can find `Rohlik_CZ_assignment.postman_collection.json` Postman test suite which can be used to 
invoke application REST endpoints.

## API description

The `docs` directory contains Swagger 2.0 API description.

## Implementation Notes
### Product API

* Product list API implemented to facilitate easier testing, although not required.
* Product name is not unique.
* Product update API (PUT) requires all product attributes to be present in the request. 
In real situation, only the attribute being updated would be present in the request.

### Order API

* PUT method with no parameters or request body was used to implement "Mark Order As Paid" operation. This would probably
 not be the case in real life, because this HTTP method would be used for other order updates as well.
* DELETE method with no parameters was used to implement "Cancel Order" operation.

### General

* The assignment didn't specify return HTTP status codes / response content. Common recommendations were followed, such as listed on https://restfulapi.net/.
* Several common-sense features were implemented, although not strictly required by the assignment, such as:
    *   validations
    *   business rules (i.e. reserved stock quantity must not exceed the actual stock quantity)
    *   order state transition checks
    *   concurrency and transactions features, locking
* The [Apache PMD Plugin](https://maven.apache.org/plugins/maven-pmd-plugin/) is integrated in the build to ensure code quality. 
It is configured to fail the build in case any violations are found.


# Original Assignment

Napište aplikaci, která bude pomocí REST rozhraní spravovat databázi produktů a objednávek.

REST operace na objednávky

*   vytvoření objednávky
*   storno objednávky
*   zaplacení objednávky

REST operace na produkty

*   vytvoření produktu
*   smazání produktu
*   aktualizace produktu - aktualizace skladové zásoby, názvu, ...

Každý produkt musí mít povinně název, množství na skladě a cenu za jednotku.
Objednávka může být na 1 až N existujících produktů a jeho libovolné jednotkové
množství (např. 5 rohlíků, 2 mléka, ...), vždy je však potřeba, aby všechny produkty, na
které objednávka vzniká, byly v dostatečném množství na skladě.

Pokud tato podmínka není splněna, odpovědí na volání endpointu na vytvoření
objednávky budou položky, u kterých není dostačující skladová zásoba včetně
chybějícího množství.

Pracujte i s tím, že objednávka produktů snižuje jejich dostupné množství pro následující
objednávky i v případě, že objednávka ještě není zaplacena, nejdéle však na dobu 30
minut, potom se musí provést nová objednávka (současná objednávka je invalidována).
Storno objednávky uvolní rezervované zboží.

Výběr technologií necháváme na vás, obecně však doporučujeme Spring Boot 2,
Tomcat, Javu 8+, H2 in-memory DB. Stejně tak necháváme na vás i samotnou realizaci
této úlohy, jako např. různé pomocné tabulky, dokumentace endpointů (např. pomocí
Swagger), testy.