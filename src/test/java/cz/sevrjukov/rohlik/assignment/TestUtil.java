package cz.sevrjukov.rohlik.assignment;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.sevrjukov.rohlik.assignment.domain.OrderState;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Order;
import cz.sevrjukov.rohlik.assignment.persistence.entities.OrderItem;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Product;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Util class for unit tests.
 */
public final class TestUtil {

    private TestUtil() {
        // prevent instantiation.
    }

    public static Product mockProduct(final long productId) {
        return Product.builder()
                .name("Product name " + productId)
                .description("Product desc " + productId)
                .price(BigDecimal.valueOf(12.34))
                .reservedQuantity(10)
                .stockQuantity(20)
                .productId(productId)
                .build();
    }

    public static Order mockOrder(final long orderOd) {
        return Order.builder()
                .orderId(orderOd)
                .createdTime(LocalDateTime.of(2020, 6, 22, 15, 22, 56))
                .orderState(OrderState.NEW).build();
    }

    public static Set<OrderItem> mockOrderItems(final Order order, final long... productIds) {
        long orderItemId = 0;
        int quantity = 5;
        final Set<OrderItem> result = new HashSet<>();
        for (final long productId : productIds) {
            result.add(new OrderItem(++orderItemId, mockProduct(productId), order, ++quantity));
        }
        return result;
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
