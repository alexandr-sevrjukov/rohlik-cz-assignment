package cz.sevrjukov.rohlik.assignment.services;

import cz.sevrjukov.rohlik.assignment.domain.ProductDto;
import cz.sevrjukov.rohlik.assignment.domain.exception.BusinessException;
import cz.sevrjukov.rohlik.assignment.persistence.OrderRepository;
import cz.sevrjukov.rohlik.assignment.persistence.ProductRepository;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.Optional;

import static cz.sevrjukov.rohlik.assignment.TestUtil.mockProduct;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ProductService.class)
public class ProductServiceTest {

    @Autowired
    private ProductService tested;

    @MockBean
    private OrderRepository orderRepositoryMock;

    @MockBean
    private ProductRepository productRepositoryMock;

    @Test
    public void testProductUpdate() {

        final long productId = 234L;
        var storedProductEntity = mockProduct(productId);
        // mock behaviour
        when(productRepositoryMock.findById(productId))
                .thenReturn(Optional.of(storedProductEntity));

        final Product modifiedEntity = mockProduct(productId);
        modifiedEntity.setStockQuantity(45);
        modifiedEntity.setReservedQuantity(56);
        modifiedEntity.setName("modified name");
        modifiedEntity.setDescription("modified description");
        modifiedEntity.setPrice(BigDecimal.valueOf(987.65));

        when((productRepositoryMock.save(modifiedEntity))).thenReturn(modifiedEntity);

        final ProductDto productDto = ProductDto.builder()
                .productId(productId)
                .name("modified name")
                .description("modified description")
                .price(BigDecimal.valueOf(987.65))
                .stockQuantity(45)
                .reservedQuantity(56)
                .build();

        // actual test
        final ProductDto savedProduct = tested.updateProduct(productId, productDto);
        assertEquals(savedProduct, productDto, "Returned and passed object must match");

        // mocks invocation verification
        Mockito.verify(productRepositoryMock, times(1)).findById(any());
        Mockito.verifyNoInteractions(orderRepositoryMock);
    }


    @Test
    public void testProductDeletionSuccess() {
        final long productId = 42L;
        final Product storedProductEntity = mockProduct(productId);
        // mock behaviour
        when(productRepositoryMock.findById(productId))
                .thenReturn(Optional.of(storedProductEntity));
        when(orderRepositoryMock.countOfOrderItemsUsingProduct(productId)).thenReturn(0);
        // actual test
        tested.deleteProduct(productId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteNonExistingProduct() {
        final long productID = 123L;
        when(productRepositoryMock.findById(productID))
                .thenReturn(Optional.empty());
        tested.deleteProduct(productID);
    }

    @Test(expected = BusinessException.class)
    public void testProductInUseDeletion() {
        final long productId = 45L;
        final Product storedProductEntity = mockProduct(productId);
        // mock behaviour
        when(productRepositoryMock.findById(productId))
                .thenReturn(Optional.of(storedProductEntity));
        when(orderRepositoryMock.countOfOrderItemsUsingProduct(productId)).thenReturn(2);
        // actual test
        tested.deleteProduct(productId);
    }

}