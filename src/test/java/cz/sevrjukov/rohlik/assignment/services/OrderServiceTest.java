package cz.sevrjukov.rohlik.assignment.services;

import cz.sevrjukov.rohlik.assignment.domain.OrderDto;
import cz.sevrjukov.rohlik.assignment.domain.OrderItemDto;
import cz.sevrjukov.rohlik.assignment.domain.OrderState;
import cz.sevrjukov.rohlik.assignment.domain.exception.BusinessException;
import cz.sevrjukov.rohlik.assignment.persistence.OrderRepository;
import cz.sevrjukov.rohlik.assignment.persistence.ProductRepository;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Order;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static cz.sevrjukov.rohlik.assignment.TestUtil.mockOrder;
import static cz.sevrjukov.rohlik.assignment.TestUtil.mockOrderItems;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

/**
 * Test suite for {@link OrderService} class.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = OrderService.class)
public class OrderServiceTest {

    @Autowired
    private OrderService tested;

    @MockBean
    private OrderRepository orderRepositoryMock;

    @MockBean
    private ProductRepository productRepositoryMock;

    /**
     * Tests successful order creation.
     */
    @Test
    public void testCreateOrderOk() {

        final OrderItemDto item1 = new OrderItemDto(1L, 5, 0);
        // this one will be low on stock:
        final OrderItemDto item2 = new OrderItemDto(2L, 6, 0);

        final OrderDto orderDto = OrderDto.builder()
                .orderItems(Set.of(item1, item2))
                .build();

        // mocks
        final Product dbProd1 = Product.builder().productId(1L).stockQuantity(50).reservedQuantity(3).build();
        final Product dbProd2 = Product.builder().productId(2L).stockQuantity(100).reservedQuantity(8).build();
        when(productRepositoryMock.findByIdAndLock(1L)).thenReturn(Optional.of(dbProd1));
        when(productRepositoryMock.findByIdAndLock(2L)).thenReturn(Optional.of(dbProd2));
        when(productRepositoryMock.findById(1L)).thenReturn(Optional.of(dbProd1));
        when(productRepositoryMock.findById(2L)).thenReturn(Optional.of(dbProd2));


        final Order persistedOrder = new Order();
        persistedOrder.setOrderId(456L);
        when(orderRepositoryMock.save(any(Order.class))).thenReturn(persistedOrder);


        // actual test
        final OrderDto result = tested.createOrder(orderDto);
        Assert.assertEquals("Order state must be INSUFFICIENT_STOCK ",
                OrderState.NEW, result.getOrderState());
        Assert.assertEquals("Order ID does not match", 456L, result.getOrderId());

    }

    /**
     * Tests attempt to create an order for non-existing product.
     */
    @Test(expected = BusinessException.class)
    public void testCreateOrderProductNotExist() {
        final OrderItemDto item1 = new OrderItemDto(1L, 5, 0);

        final OrderDto orderDto = OrderDto.builder()
                .orderItems(Set.of(item1))
                .build();

        when(productRepositoryMock.findById(1L)).thenReturn(Optional.empty());
        tested.createOrder(orderDto);
    }

    @Test(expected = BusinessException.class)
    public void testCreateOrderWithDuplicates() {
        final OrderItemDto item1 = new OrderItemDto(1L, 5, 0);
        // this one will be low on stock:
        final OrderItemDto item2 = new OrderItemDto(1L, 6, 0);

        final OrderDto orderDto = OrderDto.builder()
                .orderItems(Set.of(item1, item2))
                .build();

        tested.createOrder(orderDto);
    }

    /**
     * Tests attempt to create an order for insufficient stock items.
     */
    @Test
    public void testCreateOrderLowStock() {

        final OrderItemDto item1 = new OrderItemDto(1L, 5, 0);
        // this one will be low on stock:
        final OrderItemDto item2 = new OrderItemDto(2L, 6, 0);

        final OrderDto orderDto = OrderDto.builder()
                .orderItems(Set.of(item1, item2))
                .build();

        // mocks
        final Product dbProd1 = Product.builder().productId(1L).stockQuantity(50).reservedQuantity(3).build();
        final Product dbProd2 = Product.builder().productId(2L).stockQuantity(10).reservedQuantity(8).build();
        when(productRepositoryMock.findByIdAndLock(1L)).thenReturn(Optional.of(dbProd1));
        when(productRepositoryMock.findByIdAndLock(2L)).thenReturn(Optional.of(dbProd2));


        // actual test
        final OrderDto createdOrder = tested.createOrder(orderDto);
        Assert.assertEquals("Order state must be INSUFFICIENT_STOCK ",
                OrderState.INSUFFICIENT_STOCK, createdOrder.getOrderState());

        Assert.assertEquals("Missing items count must be 1", 1, createdOrder.getOrderItems().size());

        final OrderItemDto missingItem = createdOrder.getOrderItems().stream().findFirst().get();
        Assert.assertEquals("Missing item product it does not match", Long.valueOf(2L), missingItem.getProductId());
        Assert.assertEquals("Missing item count does not match", 4, missingItem.getMissingQuantity());
    }

    @Test
    public void testMarkOrderAsPaidOk() {
        final long orderId = 12345L;

        final Order orderMock = mockOrder(orderId);
        orderMock.setOrderItems(mockOrderItems(orderMock, 10L, 20L, 30L));
        when(orderRepositoryMock.findById(orderId)).thenReturn(Optional.of(orderMock));

        final ArgumentCaptor<Order> orderArgCaptor = ArgumentCaptor.forClass(Order.class);

        // actual test
        tested.markOrderAsPaid(orderId);

        Mockito.verify(orderRepositoryMock).save(orderArgCaptor.capture());
        Mockito.verify(productRepositoryMock, times(3)).resolveStockReservation(any(Long.class), any(Integer.class));
        Assert.assertEquals("Saved order state must be PAID", OrderState.PAID, orderArgCaptor.getValue().getOrderState());
    }

    @Test
    public void testCancelOrderOk() {
        final long orderId = 54321;

        final Order orderMock = mockOrder(orderId);
        orderMock.setOrderItems(mockOrderItems(orderMock, 11L, 12L, 13L, 14L));
        when(orderRepositoryMock.findById(orderId)).thenReturn(Optional.of(orderMock));

        final ArgumentCaptor<Order> orderArgCaptor = ArgumentCaptor.forClass(Order.class);

        // actual test
        tested.cancelOrder(orderId);

        Mockito.verify(orderRepositoryMock).save(orderArgCaptor.capture());
        Mockito.verify(productRepositoryMock, times(4)).releaseStockReservation(any(Long.class), any(Integer.class));
        Assert.assertEquals("Saved order state must be PAID", OrderState.CANCELLED, orderArgCaptor.getValue().getOrderState());
    }

    @Test
    public void expireOrdersTestOk() {

        final Order expiredOrder1 = mockOrder(200L);
        expiredOrder1.setOrderItems(mockOrderItems(expiredOrder1, 201L, 202L));

        final Order expiredOrder2 = mockOrder(300L);
        expiredOrder2.setOrderItems(mockOrderItems(expiredOrder2, 301L, 302L, 303L));

        when(orderRepositoryMock.findByCreatedTimeBeforeAndOrderState(any(LocalDateTime.class), eq(OrderState.NEW)))
                .thenReturn(List.of(expiredOrder1, expiredOrder2));

        when(orderRepositoryMock.findById(200L)).thenReturn(Optional.of(expiredOrder1));
        when(orderRepositoryMock.findById(300L)).thenReturn(Optional.of(expiredOrder2));

        final ArgumentCaptor<Order> orderArgCaptor = ArgumentCaptor.forClass(Order.class);

        // actual test
        tested.expireOldOrders();

        Mockito.verify(orderRepositoryMock, times(2)).save(orderArgCaptor.capture());
        Mockito.verify(productRepositoryMock, times(5)).releaseStockReservation(any(Long.class), any(Integer.class));

        orderArgCaptor.getAllValues().forEach(
                capturedArg ->
                        Assert.assertEquals("Saved order state must be PAID", OrderState.EXPIRED, capturedArg.getOrderState()));

    }

}


