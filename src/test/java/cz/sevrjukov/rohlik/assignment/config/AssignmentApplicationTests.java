package cz.sevrjukov.rohlik.assignment.config;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Tests application context loading.
 */
@SpringBootTest
class AssignmentApplicationTests {

    @Test
    void contextLoads() {
    }

}
