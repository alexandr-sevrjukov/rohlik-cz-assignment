package cz.sevrjukov.rohlik.assignment.persistence;

import cz.sevrjukov.rohlik.assignment.config.PersistenceConfig;
import cz.sevrjukov.rohlik.assignment.domain.OrderState;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Order;
import cz.sevrjukov.rohlik.assignment.persistence.entities.OrderItem;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static cz.sevrjukov.rohlik.assignment.TestUtil.mockProduct;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.util.Assert.notEmpty;

/**
 * Test suite for {@link OrderRepository} class.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = PersistenceConfig.class)
public class OrderRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private OrderRepository tested;

    @Autowired
    private ProductRepository productRepository;

    /**
     * Tests persisting order and its items.
     */
    @Test
    public void testOrderInsertion() {

        final Product product = mockProduct(123L);
        productRepository.save(product);

        final Order order = new Order();
        order.setCreatedTime(LocalDateTime.now());
        order.setOrderState(OrderState.NEW);
        final OrderItem orderItem = new OrderItem();
        orderItem.setProduct(product);
        orderItem.setQuantity(4);
        order.setOrderItems(Set.of(orderItem));

        final Order insertedOrder = tested.save(order);
        final Order selectedOrder = tested.findById(insertedOrder.getOrderId()).orElseThrow();
        assertEquals("Inserted and selected object are not equal", selectedOrder, insertedOrder);

        final Set<OrderItem> orderItems = selectedOrder.getOrderItems();
        notEmpty(orderItems, "Order items set shoud not be empty");
    }

    /**
     * Tests counting order items for the given product.
     */
    @Test
    @Sql("/populate_orders.sql")
    public void testFindOrdersByProduct() {

        int usedProductOrderItemsCount = tested.countOfOrderItemsUsingProduct(1L);
        assertEquals("Product should be used in two orders", 2, usedProductOrderItemsCount);

        int unusedProductOrderItemsCount = tested.countOfOrderItemsUsingProduct(2L);
        assertEquals("Product should NOT be used in any orders", 0, unusedProductOrderItemsCount);
    }

    /**
     * Tests searching for expired orders.
     */
    @Test
    @Sql("/populate_orders.sql")
    public void testFindExpiredOrders() {

        final LocalDateTime threshold = LocalDateTime.of(
                2020, 06, 10,
                10, 11, 17);

        final List<Order> result = tested.findByCreatedTimeBeforeAndOrderState(threshold, OrderState.NEW);

        assertEquals("result size must be 1", 1, result.size());
        assertEquals("wrong order selected", 1000L, result.get(0).getOrderId());
    }

}
