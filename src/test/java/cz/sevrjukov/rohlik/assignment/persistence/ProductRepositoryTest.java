package cz.sevrjukov.rohlik.assignment.persistence;


import cz.sevrjukov.rohlik.assignment.config.PersistenceConfig;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.test.util.AssertionErrors.assertEquals;

/**
 * Test suite for {@link ProductRepository} class.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = PersistenceConfig.class)
public class ProductRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository tested;

    /**
     * Tests stock reservation.
     */
    @Test
    @Sql("/populate_products.sql")
    public void testReserveStock() {
        tested.reserveStockItems(1L, 2);

        final Product product = tested.findById(1L).get();
        assertEquals("Wrong stock quantity", 7, product.getStockQuantity());
        assertEquals("Wrong reserved quantity", 6, product.getReservedQuantity());
    }

    /**
     * Tests stock reservation resolving.
     */
    @Test
    @Sql("/populate_products.sql")
    public void testResolveStockReservation() {
        tested.resolveStockReservation(1L, 2);

        final Product product = tested.findById(1L).get();
        assertEquals("Wrong stock quantity", 5, product.getStockQuantity());
        assertEquals("Wrong reserved quantity", 2, product.getReservedQuantity());
    }

    /**
     * Tests releasing stock reservation.
     */
    @Test
    @Sql("/populate_products.sql")
    public void testReleaseStockReservation() {
        tested.releaseStockReservation(1L, 2);

        final Product product = tested.findById(1L).get();
        assertEquals("Wrong stock quantity", 7, product.getStockQuantity());
        assertEquals("Wrong reserved quantity", 2, product.getReservedQuantity());
    }
}
