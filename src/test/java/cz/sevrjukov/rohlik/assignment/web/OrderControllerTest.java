package cz.sevrjukov.rohlik.assignment.web;


import cz.sevrjukov.rohlik.assignment.domain.OrderDto;
import cz.sevrjukov.rohlik.assignment.domain.OrderItemDto;
import cz.sevrjukov.rohlik.assignment.domain.OrderState;
import cz.sevrjukov.rohlik.assignment.domain.exception.BusinessException;
import cz.sevrjukov.rohlik.assignment.services.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Set;

import static cz.sevrjukov.rohlik.assignment.TestUtil.asJsonString;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
@ContextConfiguration(classes = {OrderController.class, ControllerErrorHandler.class})
public class OrderControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OrderService orderServiceMock;

    @Test
    public void testCreateOrderOk() throws Exception {

        final long newOrderId = 123L;

        final Set<OrderItemDto> orderItems = Set.of(new OrderItemDto(1L, 5, 0));
        final OrderDto orderRequest = OrderDto.builder()
                .orderItems(orderItems)
                .build();

        final OrderDto orderResponse = OrderDto.builder()
                .orderId(newOrderId)
                .orderState(OrderState.NEW)
                .build();

        // mock behaviour
        when(orderServiceMock.createOrder(orderRequest)).thenReturn(orderResponse);

        mvc.perform(post("/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(orderRequest))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.orderId").exists())
                .andExpect(jsonPath("$.orderId", is(123)));
    }


    @Test
    public void testCreateOrderLowStock() throws Exception {
        final long newOrderId = 123L;

        final Set<OrderItemDto> orderItems = Set.of(new OrderItemDto(1L, 5, 0));
        final OrderDto orderRequest = OrderDto.builder()
                .orderItems(orderItems)
                .build();

        final OrderDto orderResponse = OrderDto.builder()
                .orderId(newOrderId)
                .orderItems(Set.of(new OrderItemDto(1L, 5, 2)))
                .orderState(OrderState.INSUFFICIENT_STOCK)
                .build();

        // mock behaviour
        when(orderServiceMock.createOrder(orderRequest)).thenReturn(orderResponse);


        mvc.perform(post("/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(orderRequest))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.orderState").exists())
                .andExpect(jsonPath("$.orderState", is("INSUFFICIENT_STOCK")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.orderItems").exists())
                .andExpect(jsonPath("$.orderItems[0].productId", is(1)))
                .andExpect(jsonPath("$.orderItems[0].requestedQuantity", is(5)))
                .andExpect(jsonPath("$.orderItems[0].missingQuantity", is(2)));
    }

    @Test
    public void testMarkOrderAsPaid() throws Exception {
        final long orderId = 234L;
        // mock behaviour
        doNothing().when(orderServiceMock).markOrderAsPaid(orderId);

        mvc.perform(put("/orders/" + orderId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testCancelOrder() throws Exception {
        final long orderId = 234L;
        // mock behaviour
        doNothing().when(orderServiceMock).cancelOrder(orderId);

        mvc.perform(delete("/orders/" + orderId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


    @Test
    public void testCancelOrderInWrongState() throws Exception {
        final long orderId = 234L;
        doThrow(new BusinessException("invalid order state transition")).when(orderServiceMock).cancelOrder(orderId);
        mvc.perform(delete("/orders/" + orderId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

}
