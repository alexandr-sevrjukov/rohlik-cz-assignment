package cz.sevrjukov.rohlik.assignment.web;

import cz.sevrjukov.rohlik.assignment.domain.ProductDto;
import cz.sevrjukov.rohlik.assignment.domain.exception.BusinessException;
import cz.sevrjukov.rohlik.assignment.services.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.persistence.EntityNotFoundException;

import static cz.sevrjukov.rohlik.assignment.TestUtil.asJsonString;
import static cz.sevrjukov.rohlik.assignment.TestUtil.mockProduct;
import static cz.sevrjukov.rohlik.assignment.domain.converter.DomainConverters.toDto;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
@ContextConfiguration(classes = {ProductController.class, ControllerErrorHandler.class})
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductService productServiceMock;

    @Test
    public void testCreateProductOk() throws Exception {

        final long newProductId = 123L;

        // request object
        final ProductDto requestDto = toDto(mockProduct(0L));
        requestDto.setProductId(null); // product ID is unknown before creation

        // the object returned from the service mock
        final ProductDto responseDto = toDto(mockProduct(0L));
        responseDto.setProductId(newProductId);

        // mock behaviour
        when(productServiceMock.createProduct(requestDto)).thenReturn(responseDto);

        mvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").exists())
                .andExpect(jsonPath("$.productId", is(123)));
    }

    @Test
    public void testValidation() throws Exception {
        // request object
        final ProductDto requestDto = toDto(mockProduct(0L));
        requestDto.setProductId(null); // product ID is unknown before creation
        requestDto.setReservedQuantity(-4);
        requestDto.setName(null);

        mvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void testUpdateNonExistingProduct() throws Exception {

        final long productId = 345L;

        // request object
        final ProductDto requestDto = toDto(mockProduct(productId));

        // mock behaviour
        when(productServiceMock.updateProduct(productId, requestDto))
                .thenThrow(new EntityNotFoundException(String.format("Product with id %s not found", productId)));

        mvc.perform(put("/products/" + productId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(requestDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteProductInUse() throws Exception {
        doThrow(new BusinessException("cannot delete product in use"))
                .when(productServiceMock).deleteProduct(345L);

        mvc.perform(delete("/products/345")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

}
