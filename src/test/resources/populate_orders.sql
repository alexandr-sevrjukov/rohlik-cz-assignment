insert into products (product_id, description, name, price, stock_quantity, reserved_quantity)
values (1, 'prod 1 name', 'product that is used in orders', 12.34, 794, 441),
       (2, 'prod 2 name', 'unused product', 22.17, 4141, 635);


insert into orders(order_id, order_state, created_time)
values (1000, 'NEW', '2020-06-10 9:58:00'),
       (1001, 'NEW', '2020-06-10 10:15:56'),
       (1002, 'EXPIRED', '2020-06-10 9:15:56');



insert into order_items (order_item_id, product_id, order_id, quantity)
values (100, 1, 1000, 2),
       (101, 1, 1001, 5);

