package cz.sevrjukov.rohlik.assignment.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

/**
 * Order DTO class. Serves both as request and response object for controllers,
 * as well as for interacting with services.
 */
@Data
@Builder
public class OrderDto {

    private long orderId;

    private OrderState orderState;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NotEmpty // not empty validation for controller requests
    @Valid
    private Set<OrderItemDto> orderItems;

}
