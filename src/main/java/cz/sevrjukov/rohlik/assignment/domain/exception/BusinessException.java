package cz.sevrjukov.rohlik.assignment.domain.exception;

/**
 * Generic exception for business-related errors.
 */
public class BusinessException extends RuntimeException {
    public BusinessException(final String message) {
        super(message);
    }
}
