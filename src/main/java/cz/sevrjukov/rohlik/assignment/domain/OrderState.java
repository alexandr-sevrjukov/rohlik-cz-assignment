package cz.sevrjukov.rohlik.assignment.domain;

/**
 * Possible order states.
 */
public enum OrderState {
    INSUFFICIENT_STOCK,
    NEW,
    PAID,
    CANCELLED,
    EXPIRED
}
