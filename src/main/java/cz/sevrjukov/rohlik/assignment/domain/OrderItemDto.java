package cz.sevrjukov.rohlik.assignment.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * OrderItem DTO class. Serves both in requests and responses for controllers,
 * as well as for interacting with services.
 */
@Data
@AllArgsConstructor
public class OrderItemDto {

    @NotNull
    private Long productId;

    @Positive
    private int requestedQuantity;

    // this value should always be positive.
    // value "5" means we have 5 missing items on the stock
    private int missingQuantity;
}
