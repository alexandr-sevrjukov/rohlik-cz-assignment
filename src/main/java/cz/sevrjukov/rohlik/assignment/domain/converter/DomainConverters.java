package cz.sevrjukov.rohlik.assignment.domain.converter;

import cz.sevrjukov.rohlik.assignment.domain.ProductDto;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Product;

/**
 * Utility class for converting to/from DTOs.
 */
public final class DomainConverters {

    private DomainConverters() {
        // prevent instantiation
    }

    public static ProductDto toDto(final Product entity) {
        return ProductDto.builder()
                .productId(entity.getProductId())
                .name(entity.getName())
                .description(entity.getDescription())
                .price(entity.getPrice())
                .stockQuantity(entity.getStockQuantity())
                .reservedQuantity(entity.getReservedQuantity())
                .build();
    }

    public static Product fromDto(final ProductDto productDto) {
        return Product.builder()
                .name(productDto.getName())
                .description(productDto.getDescription())
                .stockQuantity(productDto.getStockQuantity())
                .price(productDto.getPrice())
                .reservedQuantity(productDto.getReservedQuantity())
                .build();
    }
}
