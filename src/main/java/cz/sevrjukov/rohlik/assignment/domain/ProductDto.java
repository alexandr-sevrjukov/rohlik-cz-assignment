package cz.sevrjukov.rohlik.assignment.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

/**
 * Domain object representing product.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

    private Long productId;

    @NotEmpty
    private String name;
    private String description;

    @Positive
    @NotNull
    private BigDecimal price;

    @PositiveOrZero
    @NotNull
    private Integer stockQuantity;

    @PositiveOrZero
    private int reservedQuantity;
}
