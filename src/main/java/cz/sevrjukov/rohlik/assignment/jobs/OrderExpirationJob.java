package cz.sevrjukov.rohlik.assignment.jobs;

import cz.sevrjukov.rohlik.assignment.services.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Periodical job that triggers old orders expiration.
 */
@Component
@Slf4j
public class OrderExpirationJob {

    @Autowired
    private OrderService orderService;

    @Scheduled(fixedRateString = "${expire.orders.job.periodicity.ms}")
    public void expireOrders() {
        log.info("Running order expiration job");
        orderService.expireOldOrders();
    }
}
