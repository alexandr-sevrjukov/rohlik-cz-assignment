package cz.sevrjukov.rohlik.assignment.persistence;

import cz.sevrjukov.rohlik.assignment.domain.OrderState;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * JPA entity repository for {@link Order} entity.
 */
@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

    /**
     * Counts order items using a specific product.
     *
     * @param productId product ID
     * @return count if order items using this product.
     */
    @Query("select count(*) from OrderItem item where item.product.productId = :productId")
    int countOfOrderItemsUsingProduct(long productId);

    /**
     * Finds all orders that were created before the given datetime and having the specified state.
     *
     * @param threshold  threshold timestamp
     * @param orderState order state
     * @return list of orders
     */
    List<Order> findByCreatedTimeBeforeAndOrderState(LocalDateTime threshold, OrderState orderState);

}
