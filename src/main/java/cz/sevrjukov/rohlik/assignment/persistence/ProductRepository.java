package cz.sevrjukov.rohlik.assignment.persistence;

import cz.sevrjukov.rohlik.assignment.persistence.entities.Product;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;

/**
 * JPA repository for {@link Product} entity class.
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    /**
     * Selects product for update - acquires row lock.
     *
     * @param productId product ID
     * @return Optional of product
     */
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select p from Product p where p.productId = :productId")
    Optional<Product> findByIdAndLock(Long productId);

    /**
     * Reserves stock for the given product
     *
     * @param productId  product ID
     * @param itemsCount number of items to be reserved
     */
    @Modifying
    @Query("update Product set reservedQuantity = reservedQuantity + :itemsCount where productId = :productId")
    void reserveStockItems(long productId, int itemsCount);

    /**
     * Resolves stock reservation: decreases a number of reserved items, but also number of items in the stock.
     *
     * @param productId  product ID
     * @param itemsCount number of items to be resolved
     */
    @Modifying
    @Query("update Product set reservedQuantity = reservedQuantity - :itemsCount, " +
            "stockQuantity = stockQuantity - :itemsCount  where productId = :productId")
    void resolveStockReservation(long productId, int itemsCount);

    /**
     * Releases stock items reservation (frees them). Actual stock value is not changed.
     *
     * @param productId  product ID
     * @param itemsCount number of stock items to be released from reserved.
     */
    @Modifying
    @Query("update Product set reservedQuantity = reservedQuantity - :itemsCount where productId = :productId")
    void releaseStockReservation(long productId, int itemsCount);
}
