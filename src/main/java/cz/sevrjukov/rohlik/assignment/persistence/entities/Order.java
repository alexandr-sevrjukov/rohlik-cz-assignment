package cz.sevrjukov.rohlik.assignment.persistence.entities;

import cz.sevrjukov.rohlik.assignment.domain.OrderState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ORDERS")
public class Order {

    @Id
    @Column(name = "ORDER_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long orderId;

    @Column(name = "ORDER_STATE")
    @Enumerated(EnumType.STRING)
    @NotNull
    private OrderState orderState;

    @Column(name = "CREATED_TIME", columnDefinition = "TIMESTAMP")
    @NotNull
    private LocalDateTime createdTime;

    // cascade makes sure all order items are persisted/deleted automatically
    @OneToMany(cascade = CascadeType.ALL)
    @NotEmpty
    private Set<OrderItem> orderItems;

}
