package cz.sevrjukov.rohlik.assignment.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Configures periodical jobs.
 */
@Configuration
@EnableScheduling
@ComponentScan(basePackages = "cz.sevrjukov.rohlik.assignment.jobs")
public class JobsConfig {
}
