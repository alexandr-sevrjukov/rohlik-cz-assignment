package cz.sevrjukov.rohlik.assignment.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Configures services.
 */
@Configuration
@ComponentScan(basePackages = "cz.sevrjukov.rohlik.assignment.services")
@PropertySource("classpath:application.properties")
public class ServicesConfig {
}
