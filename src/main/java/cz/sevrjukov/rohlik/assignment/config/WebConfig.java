package cz.sevrjukov.rohlik.assignment.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Configures web components (controllers and controller advices).
 */
@Configuration
@ComponentScan(basePackages = "cz.sevrjukov.rohlik.assignment.web")
public class WebConfig {
}
