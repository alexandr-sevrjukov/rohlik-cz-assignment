package cz.sevrjukov.rohlik.assignment.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Configures database, JPA, repositories.
 */
@Configuration
@EnableJpaRepositories(basePackages = "cz.sevrjukov.rohlik.assignment.persistence")
@EntityScan(basePackages = "cz.sevrjukov.rohlik.assignment.persistence.entities")
@EnableTransactionManagement
public class PersistenceConfig {
}
