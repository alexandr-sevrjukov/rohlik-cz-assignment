package cz.sevrjukov.rohlik.assignment.services;

import cz.sevrjukov.rohlik.assignment.domain.ProductDto;
import cz.sevrjukov.rohlik.assignment.domain.exception.BusinessException;
import cz.sevrjukov.rohlik.assignment.persistence.OrderRepository;
import cz.sevrjukov.rohlik.assignment.persistence.ProductRepository;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Product;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static cz.sevrjukov.rohlik.assignment.domain.converter.DomainConverters.fromDto;
import static cz.sevrjukov.rohlik.assignment.domain.converter.DomainConverters.toDto;

/**
 * Product manipulation service.
 */
@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class ProductService {

    private ProductRepository productRepository;

    private OrderRepository orderRepository;

    /**
     * Persists new product.
     *
     * @param productDto product description DTO
     * @return product DTO including newly generated ID
     */
    public ProductDto createProduct(final ProductDto productDto) {
        log.debug("Saving new product");
        Product entity = fromDto(productDto);
        entity = productRepository.save(entity);
        return toDto(entity);
    }

    /**
     * Returns list of all products.
     *
     * @return all products
     */
    public Set<ProductDto> listAllProducts() {
        log.debug("Calling list of products");
        return StreamSupport.stream(productRepository.findAll().spliterator(), false)
                .map(p -> toDto(p)).collect(Collectors.toSet());
    }

    /**
     * Updates existing product.
     *
     * @param productId  ID of the modified product
     * @param productDto product description dto
     * @return updated product dto.
     * @throws EntityNotFoundException if product with the given ID does not exist
     */
    public ProductDto updateProduct(final Long productId, final ProductDto productDto) {
        log.debug("Updating existing product with id [{}]", productId);
        final Product existingProduct = productRepository
                .findById(productId)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Product with id %s not found", productId)));

        existingProduct.setName(productDto.getName());
        existingProduct.setDescription(productDto.getDescription());
        existingProduct.setPrice(productDto.getPrice());
        existingProduct.setReservedQuantity(productDto.getReservedQuantity());
        existingProduct.setStockQuantity(productDto.getStockQuantity());

        productRepository.save(existingProduct);

        return toDto(existingProduct);
    }

    /**
     * Deletes product.
     *
     * @param productId ID of the product to be deleted.
     * @throws EntityNotFoundException if product with the given ID is not found.
     * @throws BusinessException       if the product is used in orders.
     */
    public void deleteProduct(Long productId) {
        log.debug("Deleting product with id [{}]", productId);

        // check that product exists
        final Product product = productRepository
                .findById(productId)
                .orElseThrow(() -> new EntityNotFoundException(
                        String.format("Product with id %s not found", productId)));

        // check that product is not used in any orders
        if (orderRepository.countOfOrderItemsUsingProduct(productId) > 0) {
            throw new BusinessException(String.format("Cannot delete product with id %s, " +
                    "because it is used in some orders", productId));
        }
        // delete product
        productRepository.delete(product);
    }
}
