package cz.sevrjukov.rohlik.assignment.services;

import cz.sevrjukov.rohlik.assignment.domain.OrderState;
import cz.sevrjukov.rohlik.assignment.domain.exception.BusinessException;

import java.util.Map;
import java.util.Set;

import static cz.sevrjukov.rohlik.assignment.domain.OrderState.*;

/**
 * This defines possible state transitions for orders.
 */
public final class OrderWorkflow {

    private static final Map<OrderState, Set<OrderState>> allowedTransitions;

    static {
        allowedTransitions = Map.of(
                NEW, Set.of(PAID, CANCELLED, EXPIRED)
        );
    }

    private OrderWorkflow() {
        // prevent instantiation
    }

    public static void validateTransition(final OrderState currentState, final OrderState newState, final long orderId) {
        final Set<OrderState> allowedNewStates = allowedTransitions.get(currentState);
        if (allowedNewStates == null || !allowedNewStates.contains(newState)) {
            throw new BusinessException(
                    String.format("Transition from state [%s] to state [%s] not allowed for order [%s]", currentState, newState, orderId));
        }
    }
}
