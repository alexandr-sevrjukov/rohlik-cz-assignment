package cz.sevrjukov.rohlik.assignment.services;

import cz.sevrjukov.rohlik.assignment.domain.OrderDto;
import cz.sevrjukov.rohlik.assignment.domain.OrderItemDto;
import cz.sevrjukov.rohlik.assignment.domain.OrderState;
import cz.sevrjukov.rohlik.assignment.domain.exception.BusinessException;
import cz.sevrjukov.rohlik.assignment.persistence.OrderRepository;
import cz.sevrjukov.rohlik.assignment.persistence.ProductRepository;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Order;
import cz.sevrjukov.rohlik.assignment.persistence.entities.OrderItem;
import cz.sevrjukov.rohlik.assignment.persistence.entities.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static cz.sevrjukov.rohlik.assignment.services.OrderWorkflow.validateTransition;

/**
 * Service for order management.
 */
@Service
@Slf4j
@Transactional
public class OrderService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    /**
     * Specify this in application.properties
     */
    @Value("${order.expiration.time.seconds:1800}")
    private int orderExpirationTimeSeconds;

    /**
     * Attempts to create a new order.
     * The validates the request and checks product availability and sufficient stock.
     *
     * @param order order creation request
     * @return created order
     * @throws BusinessException if requested product(s) don't exist or stock is insufficient.
     */
    public OrderDto createOrder(final OrderDto order) {

        // check there are no duplicates
        validateUniqueProducts(order);

        // Checks product availability.
        // It also locks the product DB row for this current transaction, to make sure
        // other threads don't update on the product simultaneously (pessimistic lock).
        final Set<OrderItemDto> missingProductItems = checkAndLockItemsAvailability(order);

        if (missingProductItems.isEmpty()) {
            // all requested item quantities are available in the stock
            return createOrderInternal(order);
        } else {
            // there are not enough items on the stock
            return OrderDto.builder()
                    .orderState(OrderState.INSUFFICIENT_STOCK)
                    .orderItems(missingProductItems)
                    .build();
        }
    }

    /**
     * Checks duplicate products in the order
     *
     * @param order order request
     */
    private void validateUniqueProducts(final OrderDto order) {
        // validate duplicate product IDs
        final Set<Long> productIds = order.getOrderItems()
                .stream()
                .map(OrderItemDto::getProductId)
                .collect(Collectors.toSet());

        if (productIds.size() < order.getOrderItems().size()) {
            throw new BusinessException("There are duplicate product items in the order");
        }
    }

    /**
     * Checks product availability in the stock.
     *
     * @param order order to be checked
     * @return Set of items, for which there's insufficient stock.
     * The set is empty if every product has enough items on the stock.
     */
    private Set<OrderItemDto> checkAndLockItemsAvailability(final OrderDto order) {
        return order.getOrderItems()
                .stream()
                .map(item ->
                        new OrderItemDto(item.getProductId(),
                                item.getRequestedQuantity(),
                                item.getRequestedQuantity() - availableProductItems(item.getProductId())))
                .filter(item -> item.getMissingQuantity() > 0)
                .collect(Collectors.toUnmodifiableSet());
    }

    /**
     * Returns how many available items for the given product are available.
     * This is: stock value - reserved items
     *
     * @param productId product ID
     * @return number of how many items are available.
     */
    private int availableProductItems(long productId) {
        // findById creates a lock on the product record row in DB
        final Product product = productRepository.findByIdAndLock(productId)
                .orElseThrow(() -> new BusinessException(
                        String.format("Cannot create order, product with ID [%s] does not exist",
                                productId)));
        return product.getStockQuantity() - product.getReservedQuantity();
    }

    /**
     * Creates new order and reserves items in the stock.
     *
     * @param orderDto order creation request
     * @return created order
     */
    private OrderDto createOrderInternal(final OrderDto orderDto) {

        // reserve items on the stock
        orderDto.getOrderItems().forEach(item ->
                productRepository.reserveStockItems(item.getProductId(), item.getRequestedQuantity()));

        // create JPA entities
        final Set<OrderItem> orderItems = orderDto.getOrderItems()
                .stream().map(item -> OrderItem.builder()
                        .product(productRepository.findById(item.getProductId()).get())
                        .quantity(item.getRequestedQuantity()).build())
                .collect(Collectors.toSet());

        // persist
        final Order order = new Order();
        order.setCreatedTime(LocalDateTime.now());
        order.setOrderState(OrderState.NEW);
        order.setOrderItems(orderItems);
        final Order insertedOrder = orderRepository.save(order);

        return OrderDto.builder()
                .orderState(OrderState.NEW)
                .orderId(insertedOrder.getOrderId())
                .build();
    }


    /**
     * Marks order as paid. Releases reserved stock and decreases actual stock.
     *
     * @param orderId order ID
     */
    public void markOrderAsPaid(final long orderId) {

        final Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Order with ID %s not found", orderId)));

        validateTransition(order.getOrderState(), OrderState.PAID, orderId);

        order.setOrderState(OrderState.PAID);
        orderRepository.save(order);

        // decrease stock, release reserved items
        order.getOrderItems().forEach(
                item -> productRepository.resolveStockReservation(item.getProduct().getProductId(), item.getQuantity())
        );
    }


    /**
     * Cancels order and releases reserved stock.
     *
     * @param orderId order ID
     */
    public void cancelOrder(final long orderId) {
        finalizeOrderUnsatisfactory(orderId, OrderState.CANCELLED);
    }

    /**
     * Searches for orders older than specified time (adjustable by config) and
     * expires them, releasing reserved stock.
     */
    public void expireOldOrders() {
        log.debug("Expiring old orders");

        // calculate threshold time
        final LocalDateTime threshold = LocalDateTime.now().minus(orderExpirationTimeSeconds, ChronoUnit.SECONDS);

        log.debug("Searching for orders older than {}", threshold);
        final List<Order> expiredOrders = orderRepository.findByCreatedTimeBeforeAndOrderState(threshold, OrderState.NEW);
        log.debug("Found {} orders to be expired", expiredOrders.size());

        expiredOrders.forEach(order -> {
            log.debug("Expiring order {}", order.getOrderId());
            try {
                finalizeOrderUnsatisfactory(order.getOrderId(), OrderState.EXPIRED);
            } catch (BusinessException | EntityNotFoundException ex) {
                log.error(String.format("Failed to expire order %s", order.getOrderId()), ex);
            }
        });
    }

    /**
     * Sets order to specified final state and releases stock reservation.
     *
     * @param orderId    order ID
     * @param finalState specified final state
     */
    private void finalizeOrderUnsatisfactory(final long orderId, final OrderState finalState) {

        final Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Order with ID %s not found", orderId)));

        validateTransition(order.getOrderState(), finalState, orderId);

        order.setOrderState(finalState);
        orderRepository.save(order);

        // decrease stock, release reserved items
        order.getOrderItems().forEach(item ->
                productRepository.releaseStockReservation(item.getProduct().getProductId(), item.getQuantity())
        );
    }
}
