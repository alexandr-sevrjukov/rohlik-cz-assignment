package cz.sevrjukov.rohlik.assignment.web;

import cz.sevrjukov.rohlik.assignment.domain.OrderDto;
import cz.sevrjukov.rohlik.assignment.domain.OrderState;
import cz.sevrjukov.rohlik.assignment.services.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Order manipulation REST controller.
 */
@RestController
@RequestMapping("/orders")
@Slf4j
@AllArgsConstructor
public class OrderController {

    private OrderService orderService;

    /**
     * Creates new order
     *
     * @param orderRequest order request DTO
     * @return Response entity containing the order, or an error description.
     */
    @PostMapping
    public ResponseEntity<OrderDto> createOrder(final @Valid @RequestBody OrderDto orderRequest) {
        log.debug("Calling POST to create order {}", orderRequest);

        final OrderDto orderResponse = orderService.createOrder(orderRequest);

        if (orderResponse.getOrderState() == OrderState.NEW) {
            // ok
            return new ResponseEntity<>(orderResponse, HttpStatus.CREATED);
        } else if (orderResponse.getOrderState() == OrderState.INSUFFICIENT_STOCK) {
            // insufficient stock
            return new ResponseEntity<>(orderResponse, HttpStatus.BAD_REQUEST);
        } else {
            log.error("Invalid order state " + orderResponse.getOrderState());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Marks order as paid.
     *
     * @param orderId order ID
     */
    @PutMapping("/{orderId}")
    @ResponseStatus(HttpStatus.OK)
    public void markOrderPaid(@Valid @NotNull @PathVariable("orderId") Long orderId) {
        log.debug("Calling PUT to mark order {} as paid", orderId);
        orderService.markOrderAsPaid(orderId);
    }

    /**
     * Cancel order
     *
     * @param orderId order ID
     */
    @DeleteMapping("/{orderId}")
    @ResponseStatus(HttpStatus.OK)
    public void cancelOrder(@Valid @NotNull @PathVariable("orderId") Long orderId) {
        log.debug("Calling DELETE to cancel order with Id {}", orderId);
        orderService.cancelOrder(orderId);
    }
}
