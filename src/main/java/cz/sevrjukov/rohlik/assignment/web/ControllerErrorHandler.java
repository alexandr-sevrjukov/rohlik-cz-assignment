package cz.sevrjukov.rohlik.assignment.web;

import cz.sevrjukov.rohlik.assignment.domain.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This handles various exceptions thrown by the service layer and transforms
 * them into appropriate HTTP responses.
 */
@ControllerAdvice
@Slf4j
public class ControllerErrorHandler extends ResponseEntityExceptionHandler {

    /**
     * This handles validation failures.
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
                                                                  final HttpHeaders headers,
                                                                  final HttpStatus status,
                                                                  final WebRequest request) {

        final Map<String, Object> errorsDescription = new LinkedHashMap<>();

        final List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getField() + ": " + x.getDefaultMessage())
                .collect(Collectors.toList());

        errorsDescription.put("errors", errors);
        return new ResponseEntity<>(errorsDescription, headers, status);
    }

    /**
     * This handles {@link EntityNotFoundException} exceptions.
     */
    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<Object> handleConstraintViolation(EntityNotFoundException ex) {
        logger.warn("Handling exception", ex);
        return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }


    /**
     * This handles {@link BusinessException} exceptions.
     */
    @ExceptionHandler({BusinessException.class})
    public ResponseEntity<Object> handleIllegalState(BusinessException ex) {
        logger.warn("Handling business exception", ex);
        return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
