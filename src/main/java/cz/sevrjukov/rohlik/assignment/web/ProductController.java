package cz.sevrjukov.rohlik.assignment.web;

import cz.sevrjukov.rohlik.assignment.domain.ProductDto;
import cz.sevrjukov.rohlik.assignment.services.ProductService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Product manipulation REST API controller.
 */
@RestController
@RequestMapping("/products")
@Slf4j
@AllArgsConstructor
public class ProductController {

    private ProductService productService;

    /**
     * Create new product.
     *
     * @param product product domain object
     * @return domain object, including newly assigned ID
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProductDto createProduct(final @Valid @RequestBody ProductDto product) {
        log.debug("Calling POST to create product {}", product);
        return productService.createProduct(product);
    }

    /**
     * Update existing product.
     *
     * @param product   product domain object
     * @param productId product ID
     * @return updated product domain object
     */
    @PutMapping("/{productId}")
    @ResponseStatus(HttpStatus.OK)
    public ProductDto updateProduct(final @Valid @RequestBody ProductDto product,
                                    @Valid @NotNull @PathVariable("productId") Long productId) {
        log.debug("Calling PUT to update product {} with Id [{}]", product, productId);
        return productService.updateProduct(productId, product);
    }

    /**
     * Delete existing product.
     *
     * @param productId product ID
     */
    @DeleteMapping("/{productId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteProduct(@Valid @NotNull @PathVariable("productId") Long productId) {
        log.debug("Calling DELETE with product Id {}", productId);
        productService.deleteProduct(productId);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Set<ProductDto> listAllProducts() {
        log.debug("Calling GET all products list");
        return productService.listAllProducts();
    }

}
